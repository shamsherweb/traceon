__author__ = 'shamsher'
from sqlalchemy import Column ,BIGINT,String,DateTime,Boolean,Numeric
from passlib.apps import custom_app_context as password_context
from dbhelper import Base,create_msqlengine,create_session
from flask import Flask,g
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)

app = Flask(__name__)

app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy dog'
class User(Base):
    __tablename__ = 'user'

    mobile_number = Column(BIGINT,primary_key=True)
    full_name = Column(String(30))
    email_address = Column(String(40),unique=True)
    profile_url = Column(String(100))
    hash_passcode = Column(String(128))
    gcm_registration_id = Column(String(200))
    country = Column(String(20))

    def update_profile_url(self,url):
        self.profile_url = url

    def set_gcm_registration_id(self,gcm_id):
        self.gcm_registration_id = gcm_id

    def hash_password(self,password):
        self.hash_passcode = password_context.encrypt(password)

    def verify_password(self,password):
        return password_context.verify(password,self.hash_passcode)
    
    def get_fullname(self):
        return self.full_name

    @staticmethod
    def verify_auth_token (token):
        print "verify auth token"
        print token
        print "\n"
        s = Serializer (app.config['SECRET_KEY'])
        try:
            data = s.loads (token)
            print "------------------"
            print data
            print "valid token"
        except SignatureExpired:
            print "valid token but expired"
            return None  # valid token, but expired
        except BadSignature:
            print "invalid token"
            return None  # invalid token
        #user = User.query.filter_by (username = data['id']).first ()
        user = session.query (User).filter_by (mobile_number = data['id']).first ()
        fullName = "Inside Model User" + user.full_name
        print fullName
        return user

    def generate_auth_token (self, expiration = 600):
        s = Serializer (app.config['SECRET_KEY'], expires_in = expiration)
        return s.dumps ({'id': self.mobile_number})

class Tracking(Base):
    __tablename__ = 'tracking'

    tracking_id  = Column(BIGINT,primary_key=True,autoincrement=True)
    tracker_mobile_number = Column(BIGINT)
    trackee_mobile_number = Column(BIGINT)
    tracking_status = (Column(Boolean,default=False))

class LocationHistory(Base):
    __tablename__ = 'location_history'

    location_id = Column(BIGINT,primary_key=True,autoincrement=True)
    mobile_number = Column(BIGINT)
    lattitude = Column(Numeric(10,4))
    longitude = Column(Numeric(10,4))

engine = create_msqlengine()
Base.metadata.create_all(engine)
session = create_session(engine)
