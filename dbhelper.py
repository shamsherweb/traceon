__author__ = 'shamsher'
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()



def  create_msqlengine():
    return create_engine ('mysql+pymysql://traceon123:traceon@123@localhost/traceon_ver1?charset=utf8mb4')

def create_session(engine):
    Base.metadata.bind = engine
    dbsession = sessionmaker(bind=engine)
    return dbsession()
