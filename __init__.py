from flask import Flask,request,jsonify
from view import create_user,login_user,get_all_contacts,insert_gcm_id,invite_notification,invite_list,get_all_contacts,delete_account,change_passcode,insert_gcm_id,add_contact,get_profile_information,upload_location_history,update_user
from flask.ext.httpauth import HTTPBasicAuth
from models import User,g
from werkzeug.utils import secure_filename
from flask_mail import Mail, Message
from dbhelper import create_msqlengine,create_session

app = Flask (__name__)
import  os
import base64
engine = create_msqlengine()
session = create_session(engine)

app.config.update(
	DEBUG=True,
	#EMAIL SETTINGS
	MAIL_SERVER='smtp.gmail.com',
	MAIL_PORT=465,
	MAIL_USE_SSL=True,
	MAIL_USERNAME = 'shamsher@webnoo.com',
	MAIL_PASSWORD = '@Webnoo9988'
	)

auth = HTTPBasicAuth()
mail=Mail(app)

BASE_URL = "http://www.shamsid.xyz/static/uploads/"
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER =os.path.join(APP_ROOT,'static/uploads/')
app.config['UPLOAD_FOLDER']=UPLOAD_FOLDER
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

@app.route ('/',methods=['POST'])
def hello_world ():
    mobile = request.form.get('mobile_number')
    invite_notification(mobile)
    return "test"

@app.route('/insert_gc',methods=['POST'])
def insert_gcm():
    mobile_number = request.form.get('mobile_number')
    gcm_id = request.form.get('gcm_id')
    insert_gcm_id(mobile_number,gcm_id)
    return "testing done"

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

@app.route('/traceon/api/signup',methods=['POST'])
def signup():
    mobile_number = request.form.get("mobile_number")
    email_address = request.form.get("email_address")
    country_name = request.form.get ("country")
    full_name = request.form.get ("full_name")
    pass_code = country = request.form.get ("pass_code")
    gcm_id = request.form.get("gcm_registration_id")
    user = User(mobile_number = mobile_number,email_address = email_address,country = country_name,full_name= full_name,
                gcm_registration_id=gcm_id)

    return create_user(user,pass_code)

@app.route('/traceon/api/signin',methods=['POST'])
def signin():
    mobile_number = request.form.get("mobile_number")
    pass_code = request.form.get ("pass_code")
    return login_user(mobile_number,pass_code)


@app.route('/traceon/api/add_device',methods=['POST'])
def add_device():
    trackee = request.form.get("trackee_mobile_number")
    tracker = request.form.get("tracker_mobile_number")
    
    print trackee
    print "-------------------"
    print tracker

    return add_contact(tracker,trackee)

#authentication token required
@app.route('/traceon/api/contacts',methods=['POST'])
@auth.login_required
def get_contacts():
    print request.headers["Authorization"]
    fullName = "In get Contact"+g.user.full_name
    return "test"

@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    temp = "verify_username_password"+username_or_token
    print temp
    user = User.verify_auth_token(username_or_token)
    print user
    print "inside the api and verify password"
    print username_or_token
    if not user:
        # try to authenticate with username/password
        username = "verify pass word" + username_or_token
        print username
        user = session.query(User).filter_by(mobile_number=username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True

@app.route('/traceon/api/token')
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token(600)
    return jsonify({'token': token.decode('ascii'), 'duration': 600})

@app.route('/traceon/api/get_profile_info',methods=['GET'])
@auth.login_required
def get_profile_info():
    data = request.form.get('mobile_number')
    return get_profile_information(data)

@app.route('/traceon/api/upload_location',methods=['POST'])
def upload_location():
    data = request.get_json()
    return upload_location_history(data)

@app.route('/traceon/api/update',methods=['POST'])

def update_information():
    mobile_number = request.form.get ("mobile_number")
    email_address = request.form.get ("email_address")
    full_name = request.form.get("full_name")

    return update_user(mobile_number,email_address,full_name)
@app.route('/traceon/api/change_password',methods=['POST'])
def changePassword():
    mobile_number = request.form.get ("mobile_number")
    pass_code = request.form.get ("pass_code")
    return change_passcode(mobile_number, pass_code)

@app.route('/traceon/api/update_gcm',methods=['POST'])
def update_gcm():
    mobile_number = request.form.get("mobile_number")
    gcm_id = request.form.get("gcm_id")

    insert_gcm_id(mobile_number,gcm_id)
@app.route('/traceon/api/delete_device',methods=['POST'])
def delete_contact():
    mobile_number = request.form.get("mobile_number")
    return delete_account(mobile_number)

@app.route('/traceon/api/get_all_contact',methods=['POST'])
def friendlist():
    mobile_number = request.form.get("mobile_number")
    print mobile_number
    return get_all_contacts(mobile_number)

@app.route('/traceon/api/send_email',methods=['POST'])
def send_email():
    msg = Message('Hello',sender='shamsher@gmail.com',recipients=['mail.shamshersiddiqui@gmail.com'])
    msg.body = "This is the email body"
    mail.send(msg)
    return "Sent"
    
@app.route('/traceon/api/invite_list',methods=['POST'])
def invite():
    mobile_number = request.form.get('mobile_number')

    return invite_list(mobile_number)

@app.route('/traceon/api/upload_profile',methods=['POST'])
def upload_image():
    file = request.form.get('image')
    imgdata = base64.b64decode (file)
    filename = 'some_image.jpg'  # I assume you have a way of picking unique filenames
    with open (filename, 'wb') as f:
        f.write (imgdata)

    return "test"


if __name__ == '__main__':
    app.debug = True
    app.run ()

