__author__ = 'shamsher'
from dbhelper import create_msqlengine,create_session
from models import User,Tracking,LocationHistory
from flask import abort,jsonify,Flask
from flask_pushjack import FlaskGCM
from pushjack import GCMClient
from flask_mail import Mail, Message
import json
import os

app = Flask (__name__)

app.config.update(
	DEBUG=True,
	#EMAIL SETTINGS
	MAIL_SERVER='smtp.gmail.com',
	MAIL_PORT=465,
	MAIL_USE_SSL=True,
	MAIL_USERNAME = 'shamsher@webnoo.com',
	MAIL_PASSWORD = '@Webnoo9988'
	)
mail=Mail(app)

engine = create_msqlengine()
session = create_session(engine)
GCM_API_KEY ='AIzaSyDaJlmeSC5MCt04uyN039w3AxTJ-KefIk0'
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER =os.path.join(APP_ROOT,'static/uploads/')

def create_user(user,passcode):

    if user.mobile_number is None  and passcode is None:
        abort(404) #bad request or incomplete request
    if user.mobile_number is None or passcode is None:
        abort(404) #bad request or incomplete request
    if session.query(User).filter_by(mobile_number = user.mobile_number).first() is not None:
        return jsonify ({"message": "user_created", "status_code": "505"})

    try:
        user.hash_password(passcode)
        session.add(user)
        session.commit()
        return jsonify({"message":"user_created","status_code":"201"})
    except Exception as e:
        print e
        session.rollback()
        return jsonify({"message":"transaction rollbacked","status_code":"5000"})
    finally:
        session.close()


def update_user(full_name,email_address,mobile_number):

    if full_name is None or email_address is None:
        abort(404) # user request or incomplete or bad request
    if session.query(User).filter_by(mobile_number = mobile_number).first() is None:
        abort(404) # user doesn't  exists
    try:
        session.query(User).filter(User.mobile_number ==mobile_number).update({
            'full_name':full_name,
            'email_address':email_address
        })
        session.commit()
        return jsonify({"status":"user information updated"})
    except Exception as e:
        print e.message
        session.rollback()
    finally:
        session.close()


def get_profile_information(mobile_number):
    if mobile_number is None:
        abort(404) # bad request
    try:
        user = session.query (User).filter_by (mobile_number = mobile_number).one ()
        contact_list = session.query (Tracking).filter_by (tracker_mobile_number = mobile_number).all ()
        peoples_tracking_list = session.query (Tracking).filter_by ( trackee_mobile_number= mobile_number).all ()
        no_of_contacts = len (contact_list)
        no_of_people_tracking = len(peoples_tracking_list)
        if user is None:
            return jsonify({"status":"user doesn't exists"})
        else:
            return jsonify({"status":"request accepted",
                            "full_name":user.full_name,
                            "mobile_number":user.mobile_number,
                            "email_address":user.email_address,
                            "country":user.country,
                            "profile_url":user.profile_url,
                            "no_of_contacts":no_of_contacts,
                            "no_of_people_tracking":no_of_people_tracking})
    except Exception as e:
        session.rollback()
        print e.message
        return "Rollback"

    finally:
        session.close()


def login_user(mobile_number,pass_code):
    if mobile_number is None and pass_code is None:
        abort(404) #bad request
    if mobile_number is None or pass_code is None:
        abort(404)

    user = session.query (User).filter_by (mobile_number = mobile_number).one ()

    if user is None:
        return jsonify ({"status": "user doesn't exists"})

    if user.verify_password(pass_code):
        return get_profile_information(mobile_number)
    else:
        return jsonify({"status":"request denied"})


def get_all_contacts(mobile_number):


    contact_list = session.query (Tracking).filter_by (tracker_mobile_number = mobile_number).all()

    no_of_contacts = len(contact_list)
    friend_list=[]
    for friend in contact_list:
        user = User()
        user_dict = {}
        user = session.query (User).filter_by (mobile_number = friend.trackee_mobile_number).one ()
        user_dict["mobile_number"]=user.mobile_number
        user_dict["full_name"] = user.full_name
        user_dict["email_address"] = user.email_address
        user_dict["profile_url"] = user.profile_url
        friend_list.append(user_dict)
    return json.dumps(friend_list)


def delete_user(data):
    mobile_number = data["mobile_number"]
    pass_code = data["pass_code"]
    if is_user_exists (mobile_number, pass_code):
        try:
            session.query (User).filter_by (mobile_number = mobile_number).delete ()
            session.commit ()
        except Exception as e:
            print e.message
            session.rollback()
        finally:
            session.close ()

        return jsonify ({"status": "user account deleted"})
    else:
        return jsonify ({"status": "user does't exists"})


def is_user_exists(mobile_number,pass_code):
    user = User()
    try:
        user = session.query (User).filter_by (mobile_number = mobile_number).one ()
    except Exception as e:
        print e.message
    finally:
        session.close()
    print user.full_name
    if user.verify_password (pass_code):
        return True
    else:
        return False


def add_contact(tracker,trackee):
    tracker_mobile_number = tracker
    trackee_mobile_number = trackee
    try:
        if trackee_mobile_number == tracker_mobile_number:
            return jsonify({"status_code":"302","message":"u cannot track yourself"})
        user = session.query(User).filter_by(mobile_number = trackee_mobile_number).one()
        print user
        #print user.get_fullname()
        msg = "Dear "+ user.get_fullname() +" accept the invitation to let the trace on to track you"
        send_email(user.email_address,msg)
        is_user_tracking = is_tracking (tracker_mobile_number, trackee_mobile_number)
        if is_user_tracking:
            return jsonify({"status_code":"302","message":"user is already tracking this device"})
        else:
            if user is not None:
                invite_notification (trackee_mobile_number)
                tracker = Tracking()
                tracker.tracker_mobile_number = tracker_mobile_number
                tracker.trackee_mobile_number = trackee_mobile_number
                tracker.tracking_status = False
                session.add(tracker)
                session.commit()
                return jsonify ({"status_code": "302", "message": "user added to database"})
            else:
                pass
                #return jsonify ({"status_code": "302", "message": "no such device is found"})
            #TO DO: here call the gcm server to send notification that you have receive the add notification
    except Exception as e:
        print e.message
        return jsonify ({"status_code": "302", "message": "no such device is found"})
        session.rollback()
    finally:
        session.close()

def is_tracking(tracker_mobile_number,trackee_mobile_number):
    tracking_detail = session.query(Tracking).filter_by(tracker_mobile_number=tracker_mobile_number,trackee_mobile_number=trackee_mobile_number).all()
    if tracking_detail:
        return True
    else:
        return False


def upload_location_history(location_data):
    try:
        for location in location_data:
                loc = LocationHistory()
                loc.lattitude = location["lattitude"]
                loc.longitude = location["longitude"]
                loc.mobile_number = location["mobile_number"]
                print loc.mobile_number
                session.add(loc)
                session.commit()
        return jsonify({"status":"success"})
    except Exception as e:
        print e.message


def change_passcode(mobile_number,pass_code):

    user = session.query (User).filter_by (mobile_number = mobile_number).one ()
    print user.full_name
    if user is None:
        return jsonify ({"status_code": "404","message":"user doesn't exists"})
    else:
        user.hash_passcode(pass_code)
        session.commit()
    return jsonify({"test":"successfull"});


def invite_notification(mobile_number):
    client = GCMClient (GCM_API_KEY)
    print "gcm mobile number"
    print mobile_number
    user = session.query (User).filter_by (mobile_number = mobile_number).one ()
    print user.mobile_number
    print user.gcm_registration_id
    body = user.full_name +' has requested to track you.'
    alert = 'Hello world.'
    notification = {'title': 'traceon', 'body': 'join traceon to track your friends and family', 'icon': 'icon'}

    # Send to single device.
    # NOTE: Keyword arguments are optional.
    res = client.send (user.gcm_registration_id,
                       alert,
                       notification = notification,
                       collapse_key = 'collapse_key',
                       delay_while_idle = True,
                       time_to_live = 60480)

    return res
    # Send to multiple devices by passing a list of ids.
    #client.send ([user.gcm_registration_id], alert)


def insert_gcm_id(mobile_number,gcm_id):
    try:
        session.query (User).filter (User.mobile_number == mobile_number).update ({
            'gcm_registration_id': gcm_id
        })
        session.commit()
    except Exception as e:
        print e.message
        session.rollback()
        return jsonify({"status":201,"message":"gcm id not updated"})
    finally:
        session.close()

def delete_account(mobile_number):
    try:
        session.query(User).filter_by(mobile_number = mobile_number).delete()
        session.query(Tracking).filter_by(tracker_mobile_number = mobile_number).delete()
        session.query (Tracking).filter_by (trackee_mobile_number = mobile_number).delete ()
        session.commit ()
        session.close ()
        return jsonify ({"status_code": "302","message":"device sucessfully removed"})
    except Exception as e:
        print e.message
        session.rollback()
        session.close ()
        return jsonify ({"status_code": "302", "message": "device couldn't be removed , try after sometime"})


def invite_list(mobile_number):
    contact_list = session.query (Tracking).filter_by (mobile_number = mobile_number,tracking_status=False).all ()
    friend_list =[]
    for person in contact_list:
        user = User ()
        user_dict = {}
        user = session.query (User).filter_by (mobile_number = person.trackee_mobile_number).one ()
        user_dict["mobile_number"] = user.mobile_number
        user_dict["full_name"] = user.full_name
        user_dict["email_address"] = user.email_address
        user_dict["profile_url"] = user.profile_url
        friend_list.append (user_dict)

    return json.dumps (friend_list)

def upload_location(data):
    pass
def send_email(email_address,message):
    msg = Message("traceon",sender='shamsher@gmail.com',recipients=[email_address])
    msg.body = message
    mail.send(msg)
    return "Sent"